(function() {
    'use strict';
    app.controller('errorController', errorController); 
    errorController.$inject = ['$scope', '$timeout', 'errorService'];
    
    function errorController($scope, $timeout, errorService) {
        $scope.errorService = errorService;

        $scope.showNotLoggedIn = false;
        $scope.showAlreadyLiked = false;
        $scope.showInputNotValid = false;

        $scope.$watch('errorService.notLoggedIn', function(newVal, oldVal) {
            if(errorService.notLoggedIn === true) {
                $scope.showNotLoggedIn = true;
                $timeout(function() {
                    $scope.showNotLoggedIn = false;
                    errorService.notLoggedIn = false;
                }, 5000);
            }
        });

        $scope.$watch('errorService.alreadyLiked', function(newVal, oldVal) {
            if(errorService.alreadyLiked === true) {
                $scope.showAlreadyLiked = true;
                $timeout(function() {
                    $scope.showAlreadyLiked = false;
                    errorService.alreadyLiked = false;
                }, 5000);
            }
        });

        $scope.$watch('errorService.inputNotValid', function(newVal, oldVal) {
            if(errorService.inputNotValid === true) {
                $scope.showInputNotValid = true;
                $timeout(function() {
                    $scope.showInputNotValid = false;
                    errorService.inputNotValid = false;
                }, 5000);
            }
        });

        $scope.$watch('errorService.added', function(newVal, oldVal) {
            if(errorService.added === true) {
                $scope.showAddedSuccessfully = true;
                $timeout(function() {
                    $scope.showAddedSuccessfully = false;
                    errorService.added = false;
                }, 5000);
            }
        });

    }
})();
