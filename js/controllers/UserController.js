(function() {
    "use strict";
    app.controller('UserController', UserController);
    UserController.$inject = ['$scope', '$modal', '$window', 'Facebook', 'myFb'];
    
    function UserController($scope, $modal, $window, Facebook, myFb) {
        $scope.user = {};
        $scope.loggedIn = false;

        $scope.showModal = function() {
            $modal.open({
                templateUrl: './modalContent.html',
                controller: UserController
            });
        };

        $scope.login = function() {
            Facebook.login(function(response) {
                getUserInfo();
                $window.location.reload();
            });
        };

        $scope.logout = function() {
            Facebook.logout(function(response) {
                $scope.user = {};
                $scope.loggedIn = false;
                myFb.setUser({});
                myFb.setLoggedIn(0);
            });
        };

        function getUserInfo() {
            Facebook.api('/me', function(response) {
                if(!response.error) {
                    $scope.user = response;
                    $scope.loggedIn = true;
                    myFb.setLoggedIn(1);
                    Facebook.api('/' + response.id + '/picture', function(link) {
                        $scope.user.profilePicture = link.data.url;
                        myFb.setUser($scope.user);
                    });
                } else {
                    $scope.loggedIn = false;
                    myFb.setLoggedIn(0);
                }
            });
        }

        setTimeout(getUserInfo, 2500);
    }
})();
