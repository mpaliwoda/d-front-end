(function() {
    "use strict";
    app.controller('facebookLoginController', facebookLoginController);
    facebookLoginController.$inject = ['$scope', 'facebook'];

    function facebookLoginController($scope, Facebook) {
        $scope.login = function() {
            console.log('hi');
            Facebook.login(function(response) {
                console.log(response);
            });
            console.log('bye');
        };

        $scope.getLoginStatus = function() {
            Facebook.getLoginStatus(function(repsponse) {
                if(response.status === 'connected') {
                    $scope.loggedIn = true;
                } else {
                    $scope.loggedIn = false;
                }
            });
        };

        $scope.me = function() {
            Facebook.api('/me', function(response) {
                $scope.user = response;
            });
        };
    }
})();
