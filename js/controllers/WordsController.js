(function() {
    "use strict";
    app.controller('WordsController', WordsController);

    WordsController.$inject = ['$scope','$window', '$timeout', 'words', 'twists_service', 'myFb', 'errorService'];

    function WordsController($scope, $window, $timeout, words, twists_service, myFb, errorService) {
        $scope.words = [];
        $scope.words2 = [];
        $scope.foundWords = [];
        $scope.liked = [];
        $scope.searchPhrase = '';
        $scope.notLoggedInUserAction = false;
        $scope.userInputInvalid = false;

        $scope.show = {
            liked: false,
            found: false,
            all: false,
            add: false,
            best: false,
        };

        $scope.getOneWord = function(id) {
            words.getOneWord(id).success(function(data) {
                $scope.word = data;
            }).error(function(err) {
                console.log(err);
            });
        };

        function scrollBottom() {
            $timeout(function() {
                let curr_height = $window.pageYOffset;
                var interval = setInterval(function() {
                    if(curr_height === 800) {
                        clearInterval(interval);
                    }
                    curr_height += 1;
                    $window.scrollBy(0, 1);
                }, 3);
            }, 0, false);
        }

        function setVisible(element) {
            let visible = ($scope.show[element] === true);
            for(let key of Object.keys($scope.show)) {
                $scope.show[key] = false;
            }

            if(!visible) {
                $scope.show[element] = true;
            }
        }

        function isWordNull(word) {
            return word.id === null
                && word.word === null
                && word.language === null
                && word.user === null;
        }

        function isUserInputValid() {
            if(
                $scope.word === '' || $scope.word === undefined
                || $scope.twist === '' || $scope.twist === undefined
                || $scope.childName === '' || $scope.childName ===undefined
            ){
                return false;
            }

            return true;
        }

        function setLikes(words) {
            let userId = myFb.getUser().id;
            if(userId === undefined) {
                return;
            }

            twists_service.getLikedByUser(userId).success(function(data) {
                words.forEach(function(word) {
                    if(data.indexOf(word.twist.id) !== -1) {
                        word.twist.likedByCurrentUser = true;
                    }
                });
            });
        }

        $scope.search = function() {
            words.search($scope.searchPhrase).success(function(data) {
                $scope.foundWords = data;
                setLikes($scope.foundWords);
                scrollBottom();
            });

            setVisible('found');
        };

        function createWordAndTwistFromUserInput() {
            return {
                twist: {
                    twist: $scope.twist,
                    childName: $scope.childName,
                    ranging: 0,
                    user: myFb.getUser().id
                },
                word: {
                    word: $scope.word,
                    language: 1,
                    user: myFb.getUser().id
                }
            };
        }

        $scope.addTwist = function() {
            if(!isUserInputValid()) {
                errorService.inputNotValid = true;
                return;
            }

            console.log(isUserInputValid());
            if(myFb.getUser().id !== undefined) {
                let wordTwist = createWordAndTwistFromUserInput();
                let word = wordTwist.word;
                let twist = wordTwist.twist;

                words.getWordByName($scope.word).success(function(data) {
                    if(isWordNull(data)) {
                        words.addWord(word).success(function(data) {
                            twist.wordId = data.id;
                            twists_service.addTwist(twist).success(function(data) {
                                $scope.words.push(data);
                            });
                        });
                    } else {
                        twist.wordId = data.id;
                        twists_service.addTwist(twist).success(function(data) {
                             // $scope.showAllWords(false);
                        });
                    }
                    errorService.added = true;
                }).error(function(err) {
                    console.log(err);
                });
            } else {
                errorService.notLoggedIn = true;
            }
        };

        $scope.showAllWords = function() {
            if(!$scope.show.all) {
                words.getAllWords().success(function(data) {
                    $scope.words = data;
                    setLikes($scope.words);
                    scrollBottom();
                }).error(function(err) {
                    console.log(err);
                });
            }

            setVisible('all');
        };

        $scope.showMostLiked = function() {
            if(!$scope.show.best) {
                words.getBest().success(function(data) {
                    $scope.words2 = data;
                    setLikes($scope.words2);
                    scrollBottom();
                });
            }
            setVisible('best');
        };

        $scope.logout = function() {
            myFb.logout();
        };

        $scope.showAdd = function() {
            setVisible('add');
            scrollBottom();
        }

        $scope.getLiked = function() {
            let userId = myFb.getUser().id;
            if(userId === undefined) {
                errorService.notLoggedIn = true;
                return;
            }

            if(!$scope.show.liked) {
                words.getLiked(userId).success(function(data) {
                    $scope.liked = data;
                    $scope.liked.forEach(function(word) {
                        word.twist.likedByCurrentUser = true;
                    });
                   scrollBottom();
                });
            }

            setVisible('liked');
        };
    }
})();
