(function() {
    "use strict";
    app.controller('helpController', helpController);
    helpController.$inject = ['$scope', '$modal'];

    function helpController($scope, $modal) {
        $scope.showHelp = function() {
            $scope.$modalInstance = $modal.open({
                scope: $scope,
                templateUrl: "help.html",
                controller: helpController
            });
        };

        $scope.closeHelp = function() {
            $scope.$modalInstance.close();
        }
    }
})();
