(function() {
    "use strict";
    // let api = "http://192.168.0.5:8000/word/";

    app.factory('words', words);
    words.$inject = ['$http', 'api'];

    function words($http, api) {
        let service = {
            getAllWords: getAllWords,
            getOneWord: getOneWord,
            getWordByName: getWordByName,
            addWord: addWord,
            search: search,
            getBest: getBest,
            getLiked: getLiked,
        };

        return service;

        function getAllWords() {
            return $http.get(api.words.getAll).success(function(data) {
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }

        function getOneWord(id) {
            return $http.get(api.words.getOne + id).success(function(data) {
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }

        function getWordByName(word) {
            return $http.get(api.words.getWordByName + word).success(function(data) {
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }

        function addWord(word) {
            return $http.post(api.words.addOne, word).success(function(data) {
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }

        function getBest() {
            return $http.get(api.words.getBest).success(function(data) {
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }

        function getLiked(userId) {
            return $http.get(api.words.getLikedByUser + userId).success(function(data) {
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }

        function search(phrase) {
            return $http.get(api.others.search + phrase).success(function(data) {
                console.log(data);
                return data;
            }).error(function(err) {
                console.log(err);
            });
        }
    }
})();
