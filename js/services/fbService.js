(function() {
    "use strict";
    app.factory('myFb', myFb);
    myFb.$inject = ['Facebook'];

    function myFb(Facebook) {
        var user = {};
        var loggedIn = 0;

        let service = {
            setUser: setUser,
            getUser: getUser,
            setLoggedIn: setLoggedIn,
            getLoggedIn: getLoggedIn,
        };
        return service;

        function setUser(User) { user = User; }
        function getUser() { return user; }
        function setLoggedIn(LoggedIn) { loggedIn = LoggedIn; }
        function getLoggedIn() { return loggedIn; }
     }
})();
