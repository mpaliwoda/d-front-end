(function() {
    "use strict";
    app.factory('api', api);
    let link = 'http://192.168.0.101:8000/';

    function api() {
        let service = {
            words: words(),
            twists: twists(),
            others: misc(),
        };

        return service;

        function words() {
            let wrds = {
                getAll: link + 'word/get_all',
                getOne: link + 'word/get_one/',
                getWordByName: link + 'word/get_by_name/',
                addOne: link + 'word/add_one',
                getBest: link + 'get_most_liked',
                getLikedByUser: link + 'word/get_liked_by/',
            };

            return wrds;
        }

        function twists() {
            let twsts = {
                addOne: link + 'twist/add_one',
                upvote: link + 'twist/upvote/',
                getLikedByUser: link + 'twist/get_liked_by/',
            };

            return twsts;
        }

        function misc() {
            let others = {
                search: link + 'search/'
            };

            return others;
        }
    }
})();
