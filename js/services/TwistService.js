(function() {
    "use strict";
    let api = "http://192.168.0.5:8000/twist";

    app.factory('twists_service', twists_service);
    twists_service.$inject = ['$http', 'api'];

    function twists_service($http, api) {
        let service = {
            addTwist: addTwist,
            upvote: upvote,
            getLikedByUser: getLikedByUser,
        };

        return service;

        function addTwist(twist) {
            return $http.post(api.twists.addOne, twist)
                .success(function(data) {
                    return data;
                }).error(function(err) {
                    console.log(err);
                });
        }

        function upvote(twistId, userId) {
            return $http.get(api.twists.upvote + twistId + '/' + userId)
                .success(function(data) {
                    return data;
                }).error(function(err) {
                    console.log(err);
                });
        }

        function getLikedByUser(userId) {
            return $http.get(api.twists.getLikedByUser + userId)
                .success(function(data) {
                    return data;
                }).error(function(err) {
                    console.log(err);
                });
        }
    }
})();
