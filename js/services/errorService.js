(function() {
    "use strict";

    app.factory('errorService', errorService);

    function errorService() {
        let service = {
            alreadyLiked: alreadyLiked,
            notLoggedIn: notLoggedIn,
            inputNotValid: inputNotValid,
            added: added,
        }
        
        var alreadyLiked = false;
        var notLoggedIn = false;
        var inputNotValid = false;
        var added = false;

        return service;
    }
})();
