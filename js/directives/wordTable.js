(function() {
    "use strict";
    app.directive('mytable', ['$compile', 'twists_service', 'myFb', 'errorService', function($compile, twists_service, myFb, errorService) {
        return {
            restrict: 'E',
            scope: {
                words: '=',
            },
            templateUrl: 'js/directives/wordtmpl.html',
            link: function(scope) {
                scope.reverse = false;
                scope.propertyName = 'word';

                scope.setSortingBy = function(elem) {
                    scope.propetyName = elem;
                    scope.reverse = !scope.reverse;
                }

                scope.upvote = function(twist) {
                    let user_id = myFb.getUser().id;
                    if(user_id === undefined) {
                        errorService.notLoggedIn = true;
                        return;
                    }

                    twists_service.upvote(twist.id, user_id).success(function(data) {
                        if(data === '"add"') {
                            twist.ranging = parseInt(twist.ranging);
                            twist.ranging += 1;
                            twist.likedByCurrentUser = true;
                        } else if(data === '"delete"'){
                            twist.ranging = parseInt(twist.ranging);
                            twist.likedByCurrentUser = false;
                            if(twist.ranging > 0)
                                twist.ranging -= 1;
                        } else {
                            return;
                        }
                    });
                };
            }
        }
    }]);
})();
