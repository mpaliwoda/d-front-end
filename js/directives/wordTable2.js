(function() {
    "use strict";
    app.directive('mytable2', ['$compile', 'twists_service', 'myFb', 'errorService', function($compile, twists_service, myFb, errorService) {
        return {
            restrict: 'E',
            scope: {
                words: '=',
            },

            templateUrl: 'js/directives/wordtmpl2.html',
            link: function(scope, e, a) {
            scope.filtered = [];
            scope.currentPage = 1;
            scope.numPerPage = 10;
            scope.maxSize = 5;

            scope.reverse = true;
            scope.propertyName = 'ranging';

            scope.setSortingBy = function(elem) {
                scope.propetyName = elem;
                scope.reverse = !scope.reverse;
            };

            scope.$watch('currentPage + numPerPage', function() {
                var begin = ((scope.currentPage - 1) * scope.numPerPage);
                var end = begin + scope.numPerPage;
                
                scope.filtered = scope.words.slice(begin, end);
                console.log(scope.filtered);
            });
            
            scope.$watch('words', function(newVal, oldVal) {
                if(oldVal.length === 0 || newVal.length !== 0) {
                    scope.filtered = scope.words.slice(0, 10);
                }
            });


            scope.upvote = function(word) {
                console.log(scope.words);
                let user_id = myFb.getUser().id;
                if(user_id === undefined) {
                    errorService.notLoggedIn = true;
                    return;
                }

                twists_service.upvote(word.twist.id, user_id).success(function(data) {
                    if(data === '"add"') {
                        word.twist.ranging = parseInt(word.twist.ranging);
                        word.twist.ranging += 1;
                        word.twist.likedByCurrentUser = true;
                    } else if(data === '"delete"'){
                        word.twist.ranging = parseInt(word.twist.ranging);
                        word.twist.likedByCurrentUser = false;
                        if(word.twist.ranging > 0)
                            word.twist.ranging -= 1;
                    } else {
                        return;
                    }
                });
            };
        }
    };
    }]);
})();
